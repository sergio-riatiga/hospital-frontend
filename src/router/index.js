import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AgregarPaciente from '../components/AgregarPaciente.vue'
import MostrarPacientes from '../components/MostrarPacientes.vue' 

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/agregarPaciente',
    name: 'agregarpaciente',
    component: AgregarPaciente
  },
  {
    path: '/mostrarPacientes',
    name: 'mostrarPacientes',
    component: MostrarPacientes
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
